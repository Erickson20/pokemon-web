import React from 'react';
import App from './App';
import {Route, Switch} from 'react-router-dom';


import Page404 from './component/404/page404';
import Home from './component/Home/home';
import Entrenadores from './component/entrenadores/entrenadores';
import Pokemons from './component/pokemons/pokemons';
import Login from './component/Login/login';
import Register from './component/register/register';
import Detail from "./component/DetailEntrenador/Detail";
const AppRoutes = () =>
<App>
    <Switch>
        
        <Route exact path="/home" component={Home}/>
        <Route exact path="/entrenadores" component={Entrenadores}/>
        <Route exact path="/pokemons" component={Pokemons}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/Detail" component={Detail}/>
        <Route component={Page404}/>
    </Switch>
</App>;

export default AppRoutes;