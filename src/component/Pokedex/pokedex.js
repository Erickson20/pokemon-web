


import React, { Component } from 'react';
import {  Card, Row, Col} from 'mdbreact';


class pokedex extends Component {
    constructor(props) {
        super()
    }
  render() {
    return (
       
       <Row className="card-padding">
        <Col md="6">
                <div className="card mx-xl-5 danger-color-dark">
                <div className="card-body">
                <form>
                    <p className="h4 text-center py-4 white-text">Busca un pokemon</p>
                    <br /><br /><br />
                    <label htmlFor="defaultFormCardNameEx" className="white-text font-weight-light">Nombre del Pokemon</label>
                    <br />
                    <input type="text" id="defaultFormCardNameEx" className="form-control" />
                    <br /><br />
                   
                    <div className="text-center py-4 mt-3">
                    <button className="btn btn-outline-white btn-lg btn-block" type="submit">Buscar<i className="fa fa-paper-plane-o ml-2"></i></button>
                    </div>
                </form>
                </div>
            </div>
        </Col>
        <Col  md="6">
        <Card color="danger-color-dark" className="card-padding">
            <img className="img-fluid" alt="sdffd"  src="https://pbs.twimg.com/profile_images/980544485108064256/jfiHHXbR_400x400.jpg"/>
        </Card>
        </Col>
       
       </Row>
      
       
    );
  }
}

export default pokedex;
