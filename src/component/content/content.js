import React, { Component } from 'react';

class content extends Component {
  render() {
      const {body} = this.props
    return (
    <div className="content">
        {body}
    </div>
    )
  }
}

export default content;
