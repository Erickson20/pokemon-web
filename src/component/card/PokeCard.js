import React, { Component } from 'react';
import { Button, Card, CardBody, CardImage, CardTitle, CardText} from 'mdbreact';
import  './PokeCard.css';

class PokeCard extends Component {
    constructor(props) {
        super()
    }
  render() {
      
      
    return (
       
        <Card wide  >
            <CardImage cascade  src={this.props.img} width="500" height="300" />
            <CardBody cascade>
                <CardTitle>{this.props.title}</CardTitle>
                {
                    this.props.level ?
                    <CardText>level: {this.props.level? this.props.level : ''} || Tipo: {this.props.type? this.props.type : ''}</CardText>
                    :
                    <CardText>user: {this.props.user? this.props.user : ''}</CardText>
                }
                
                
                <Button color="red" href="/Detail">Ver</Button>
            </CardBody>
        </Card>
      
       
    );
  }
}

export default PokeCard;
