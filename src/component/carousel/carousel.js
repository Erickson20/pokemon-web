import React, { Component } from 'react';
import { Button, Card, CardBody, CardImage, CardTitle, CardText } from 'mdbreact';
class carousel extends Component {
  render() {
    return (
        <Card reverse>
            <CardImage cascade className="img-fluid" src="https://kaggle2.blob.core.windows.net/datasets-images/2619/4359/e3ef5846d64dc9a747afd82273456328/data-original.jpg"  />
            <CardBody cascade>
                <CardTitle>Bienvenido a PokeLoco!</CardTitle>
                <CardText>Entra y aprende a ser un entrenador loco en la vida! te aseguramos matarte en 3 dia de la loquera!!</CardText>
                <Button color="blue" href="/register">Ser un Entrenador</Button>
                <Button color="red" href="/entrenadores">Buscar Entrenador</Button>
            </CardBody>
        </Card>
    );
  }
}

export default carousel;