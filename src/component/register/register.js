import React from 'react';
import { Container, Row, Col, Input, Button, Card, CardBody} from 'mdbreact';

class login extends React.Component  {

  componentWillMount() {
    fetch('http://se.servicios.gob.do:84/api/solicitudes/nueva', {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer K3wgdl4T6r9gkcER1FSYvvcR1VqG9QpU4aasJpoI48bOvQOMtR439NAv6qgR'
    },
    body: JSON.stringify({
      numero_identidad: "40200630438",
      siglas_universidad: "UAPA",
      vip:1,
      documentos:
      [
               {
               id: "525",
               cantidad:2
           },
               {
               id: "518",
               cantidad:1
           },
               {
               id: "520",
               cantidad:1
           }
      ]
    })
    }).then(response => response.json())
    .then(data => console.log(data))
}
  render() {
    return(
      <Container className="card-padding"  >
        <section className="form-blue">
          <Row>
          <Col md="6">
              <img className="img-fluid " width="290" alt="chchc" src="https://pm1.narvii.com/6354/c11e36230bfb135687d90d891eba9829f96ef80b_hq.jpg"/>
            </Col>
            <Col md="6">
            <Card>
            <CardBody>
              <form>
                <p className="h4 text-center py-4">Conviertete en Entrenador!</p>
                <div className="grey-text">
                  <Input label="Nombre" group type="text" validate error="wrong" success="right"/>
                  <Input label="Email"  group type="email" validate error="wrong" success="right"/>
                  <Input label="Password"  group type="text" validate error="wrong" success="right"/>
                  <Input label="Numero"  group type="password" validate/>
                </div>
                <div className="text-center py-4 mt-3">
                  <Button className="btn btn-outline-blue" type="submit">Registrate</Button>
                </div>
              </form>
            </CardBody>
          </Card>
            </Col>
            
          </Row>
        </section>
      </Container>
    );
  }
};

export default login;