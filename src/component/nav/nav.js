import React, { Component } from 'react';
import { Fa,  Col, Navbar,  NavbarNav,  NavItem, NavLink} from 'mdbreact';
class nav extends Component {
  render() {
    return (
        <Navbar color="blue" dark expand="md" scrolling>
            
            <NavbarNav left>
            <NavItem>
                
                <NavLink to="/home"><Fa icon="home" size="1x"/> Home</NavLink>
              
              </NavItem>
              
              <NavItem>
                
                <NavLink to="/entrenadores"><Fa icon="hand-peace-o" size="1x"/> Entrenadores</NavLink>
              
              </NavItem>
              <NavItem>
                
                <NavLink to="/pokemons"><Fa icon="leaf" size="1x"/> pokemon</NavLink>
              
              </NavItem>
            </NavbarNav>
            <NavbarNav >
              
              
                <Col md="12">
                <input type="text" id="exampleForm2" placeholder="Buscar pokemons..." size="2" className="form-control"/>
                </Col>
                
              
            </NavbarNav>
            <NavbarNav right>
              <NavItem>
                
                <NavLink to="/login"><Fa icon="user" size="1x"/> Login</NavLink>
              
              </NavItem>
              <NavItem>
                
                <NavLink to="/register"><Fa icon="address-card" size="1x"/> Register</NavLink>
              </NavItem>
            </NavbarNav>
         
            </Navbar>
         
    );
  }
}

export default nav;
