import React, { Component } from 'react';
import PokeCard from '../card/PokeCard';
import { Col, Row, CardGroup, } from 'mdbreact';


class pokemons extends Component {

  constructor(props) {
    super()
    this.state = {
      Entrenador: [],
      Pokemons: []
    }
  }

  componentDidMount() {
    
      fetch('http://localhost:4000/api/pokemon')
      .then(response => response.json()
      .then(data =>{
        
        this.setState({ Pokemons: data.Pokemon })
      }))
        
  }

  Pokemon(data) {
    
    return data.map( (datos, index) =>(<Col className="card-padding" md="3"><PokeCard key={index} level={datos.level} type={datos.type} title={datos.name} img={datos.img}/></Col>))
  }
  render() {
    return (
      <CardGroup className="card-padding">
      <Row>
        {this.Pokemon(this.state.Pokemons)}
      </Row>
    </CardGroup>

    );
  }
}


export default pokemons;