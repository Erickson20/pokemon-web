import React, { Component } from 'react';
//import { Row, Col} from 'mdbreact';

import Select2 from './select2';
class page404 extends Component {

  constructor(props){
    super()
    this.state = {
      select: [],
      father:[]
      
    }
  }



  componentWillMount(){
    let select = [];
    let father = [];
    //cambiar la varible #str por retorno de json para probar con forms desde la api
    var str = '[ { "type": "text", "label": "Nombre entidad: ", "orden": "14", "required": true, "name": "texto25502", "subtype": "text" }, { "type": "text", "label": "Dirección Entidad (calle / no.):", "orden": "15", "required": false, "name": "texto25503", "subtype": "text" }, { "type": "text", "label": "Teléfono Entidad:", "orden": "19", "required": false, "name": "texto5009", "subtype": "text" }, { "type": "file", "label": "Documento anexo 1", "orden": "214", "required": false, "name": "sivarc1" }, { "type": "file", "label": "Documento anexo 2", "orden": "215", "required": false, "name": "sivarc3" }, { "type": "file", "label": "Documento anexo 3", "orden": "216", "required": false, "name": "sivarc4" }, { "type": "checkbox-group", "label": "Solicitante Anónimo", "orden": "2", "name": "logico02", "values": [ { "label": "Solicitante Anónimo", "value": "logico02" } ] }, { "type": "select", "entity": "TipoSolicitud", "required": true, "label": "Tipo de Solicitud", "orden": "1", "name": "texto5001", "values": [ { "label": "Quejas", "value": "01" }, { "label": "Consultas", "value": "04" }, { "label": "Reclamaciones", "value": "02" }, { "label": "Denuncias", "value": "03" } ] }, { "type": "select", "entity": "provincia", "required": false, "label": "Provincia entidad", "orden": "16", "name": "texto5031", "values": [ { "label": "Distrito Nacional", "value": "DO01" }, { "label": "Azua", "value": "DO02" } ] }, { "type": "select", "entity": "municipio", "father": "provincia", "required": false, "label": "Ciudad entidad", "orden": "17", "name": "texto5033", "values": [ { "label": "Santo Domingo de Guzmán", "value": "DO0101" }, { "label": "Estebanía", "value": "DO0202" }, { "label": "Azua de Compostela", "value": "DO0201" } ] }, { "type": "select", "entity": "sector", "father": "municipio", "required": false, "label": "Sector entidad", "orden": "18", "name": "texto5035", "values": [ { "label": "Palma Real", "value": "DO0101-0004" }, { "label": "Los Paralejos", "value": "DO0101-0001" }, { "label": "Los Angeles", "value": "DO0101-0002" }, { "label": "Los Girasoles", "value": "DO0101-0003" } ] }, { "type": "textarea", "label": "Descripción de la solicitud", "orden": "20", "required": true, "name": "texto400001" } ]'
    var json = JSON.parse(str); 
    
    

    //filtramos los select
    for (const iterator of json) {
      if(iterator.type ==='select'){
        select.push(iterator);
      }
    }
    
    
     //sacamos todos los padres posbile 
    for (const iterator of select) {       
        father[iterator['entity']] = '00';
    }
    

    //seteamos los select ylos padres
    this.setState({
      select: select,
      father: father
    })
  }

  

  render() {
    
    //console.log(this.state.select);
    
    return (

       <div className="card-padding">

        <Select2 data={this.state.select} father={this.state.father}/>

        
       </div>
    );

   
  }


  


 
}









/*
<div className="card-padding">
        <Row>
          <Col md="12">
            <h1 className="text-center">Lo siento esta pagina no esta disponible!</h1>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <img alt="sf" src="https://nerdist.com/wp-content/uploads/2017/12/bulbasaur-charmander-squirtle-pokemon.jpg" className="img-fluid"/>
          </Col>
        </Row>
       </div>

*/

 /*opcional select simple [pais]
      let pais  = {type: 'select', label: 'pais', name: 'texto5001', value: [{label: 'Estados Unidos', value: 'USA'},{label: 'Republica Dominicana', value: 'RD'},{label: 'Puerto Rico', value: 'PR'}]}
     
      let provincia = {type: 'select', label: 'provincia', name: 'texto5001', value: [{label: 'Azual', value: '01'},{label: 'Santo domingo', value: '02'},{label: 'Santiago', value: '03'}]}
      let municipio = {type: 'select', label: 'municipio', name: 'texto5002', father: 'provincia', value: [{  label: 'estafonia', value: '0101'},{  label: 'cabo abajo', value: '0102'},{  label: 'distrito nacional', value: '0201'},{  label: 'santo domingo este', value: '0202'}, { label: 'canoa alta', value: '0301'}]}
      
      let sector = {type: 'select', label: 'sector', name: 'texto5003', father: 'municipio', value: [{ label: 'eslobenia', value: '010101'},{ label: 'villa faro', value: '020201'},{  label: 'san isidro', value: '020202'},{  label: 'anancoda abajo', value: '030101'}]}
      //opcional select jerarquico [Residencial] hijo de sector
      //let Residencial = {type: 'select', label: 'Residencial', name: 'texto5004', father: 'sector', value: [{ label: 'amanda 1', value: '01010101'},{ label: 'cacao', value: '02020101'}]}
      
      
      //agregar y eleminar select deseados 
      let select = [pais,provincia,municipio,sector]
      
        */
export default page404;