import React, { Component } from 'react';
import PokeCard from '../card/PokeCard';
import { Col, Row, CardGroup } from 'mdbreact';


class entrenadores extends Component {
  constructor(props) {
    super()
    this.state = {
      Entrenador: [],
      error: ''
      
    }
  }

  componentDidMount() {
    fetch('http://localhost:4000/api/entrenador')
      .then(response => response.json()
      .then(data =>{
        
        this.setState({ Entrenador: data.Entrenadores })
        
      })).catch(err =>{
        this.setState({
          error: 'error al cargar los entrenadores'
        })
      })
    
        
  }


  Entrenador(data) {
    
    return data.map( (datos, index) =>(<Col className="card-padding" md="3"><PokeCard key={index} user={datos.user} id={datos._id} title={datos.name} img={datos.img}/></Col>))
  }

  error(){
    return (<h1>error al cargar los entrenadores</h1>)
  }
  render() {
    return (
      <CardGroup className="card-padding">
      <Row>
        {this.Entrenador(this.state.Entrenador)}
        <center><h1>{this.state.error}</h1></center>
      </Row>
    </CardGroup>

    );
  }
}

export default entrenadores;