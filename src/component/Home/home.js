import React, { Component } from 'react';
import Carousel from '../carousel/carousel';

import PokeCard from '../card/PokeCard';

import { Col, Row, CardGroup,Card } from 'mdbreact';
class home extends Component {

      constructor(props) {
        super()
        this.state = {
          Entrenador: [],
          Pokemons: []
        }
      }

  componentDidMount() {
    fetch('http://localhost:4000/api/entrenador')
      .then(response => response.json()
      .then(data =>{
        
        this.setState({ Entrenador: data.Entrenadores })
        
      }))
      fetch('http://localhost:4000/api/pokemon')
      .then(response => response.json()
      .then(data =>{
        
        this.setState({ Pokemons: data.Pokemon })
      }))
        
  }

  Entrenador(data) {
    
    return data.map( (datos, index) =>(<Col className="card-padding" md="3"><PokeCard key={index} user={datos.user}  title={datos.name} img={datos.img}/></Col>))
  }

  Pokemon(data) {
    
    return data.map( (datos, index) =>(<Col className="card-padding" md="3"><PokeCard key={index} level={datos.level} type={datos.type} title={datos.name} img={datos.img}/></Col>))
  }
  render() {
    
    return (

        <div>
        <Carousel />
        <Card color="red " className="card-padding"><h3 className="text-left white-text">Nuevos Pokemons</h3></Card>
        <CardGroup className="card-padding">
          <Row>
             {this.Pokemon(this.state.Pokemons)}
          </Row>
        </CardGroup>

        <Card color="blue " className="card-padding"><h3 className="text-right white-text">Nuevos Entrenadores</h3></Card>
        <CardGroup className="card-padding">
          <Row>
          {this.Entrenador(this.state.Entrenador)} 
          </Row>
        </CardGroup>

        <Card color="green" className="card-padding"><h3 className="text-left white-text">Mapa Pokemon</h3></Card>
          <CardGroup className="card-padding">
          <Card className="card-padding">
            <img className="img-fluid" alt="dsf"  src="https://vignette.wikia.nocookie.net/es.pokemon/images/7/7e/Mapa_de_Kanto_se%C3%B1alizado_RFVH.png/revision/latest?cb=20081229145445"/>
          </Card>
          </CardGroup>
          <Card color="brown" className="card-padding"><h3 className="text-right white-text">Pokemon Films</h3></Card>
            
         
          <div className="card-padding">
           
          
          </div>
            
            
        </div>
    );
  }
}

export default home;