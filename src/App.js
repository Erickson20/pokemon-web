import React, { Component } from 'react';
import Nav from './component/nav/nav';
import Footer from './component/footer/footer';
import Content from './component/content/content';
import './App.css';

class App extends Component {
  render() {
    const {children} = this.props
    return (
      <div className="App">
        <Nav/>
          <Content body={children}/>
        
        <Footer/>
        </div>
    );
  }
}

export default App;
