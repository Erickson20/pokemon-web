import React from 'react';
import {render} from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import AppRoutes from './routes';
import './index.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';

import registerServiceWorker from './registerServiceWorker';

render(
    <Router>
        <AppRoutes/>
    </Router>
    , document.getElementById('root'));
registerServiceWorker();
